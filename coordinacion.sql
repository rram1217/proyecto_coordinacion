-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 11-10-2018 a las 19:16:24
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `coordinacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambiente`
--

CREATE TABLE `ambiente` (
  `cod_ambiente` int(15) NOT NULL,
  `ambiente` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `cod_sede` int(15) NOT NULL,
  `estado` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `ficha_manana` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `ficha_tarde` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `ficha_noche` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ambiente`
--

INSERT INTO `ambiente` (`cod_ambiente`, `ambiente`, `cod_sede`, `estado`, `ficha_manana`, `ficha_tarde`, `ficha_noche`, `tipo`) VALUES
(1, '122', 1, 'inactivo', '12', '22', 'null', '2'),
(2, '11', 1, 'inactivo', '13', '14', '1366055', 'sistemas'),
(3, '123', 1, 'inactivo', '12', 'null', '1366055', 'sistemas'),
(4, '205', 1, 'inactivo', 'null', 'null', 'null', 'sistemas'),
(5, '300', 2, 'activo', '220', '13', 'null', 'sistemas'),
(7, '2h', 7, 'activo', 'null', 'null', 'null', 'ganado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `login` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`login`, `password`) VALUES
('admin', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formacion`
--

CREATE TABLE `formacion` (
  `ficha` int(15) NOT NULL,
  `nombre_ficha` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `codigo_proyecto` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `version_programa` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `nivel` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` text COLLATE utf8_spanish_ci NOT NULL,
  `visita` date NOT NULL,
  `oferta` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin_lectiva` date NOT NULL,
  `fecha_fin_formacion` date NOT NULL,
  `cedula` int(15) NOT NULL,
  `cod_municipio` int(15) NOT NULL,
  `n_aprendices` int(15) NOT NULL,
  `aprendiz_lider` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_ambiente` int(15) NOT NULL,
  `cod_sede` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `jornada` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `medio_ambiente` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `comunicacion` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `etica` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `matematica` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `cultura_fisica` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `ingles` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `formacion`
--

INSERT INTO `formacion` (`ficha`, `nombre_ficha`, `codigo_proyecto`, `version_programa`, `nivel`, `estado`, `visita`, `oferta`, `fecha_inicio`, `fecha_fin_lectiva`, `fecha_fin_formacion`, `cedula`, `cod_municipio`, `n_aprendices`, `aprendiz_lider`, `cod_ambiente`, `cod_sede`, `jornada`, `medio_ambiente`, `comunicacion`, `etica`, `matematica`, `cultura_fisica`, `ingles`) VALUES
(11, 'adsi', '12', '12', 'tecnico', 'inactivo', '2018-09-09', '1', '2018-09-09', '2018-09-13', '2018-11-09', 1007029945, 54001, 16, 'asaq', 1, '', 'manana', 'realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada'),
(12, 'adsi', '12', '21', 'tecnico', 'activo', '2017-09-09', '2', '2017-09-09', '2018-09-09', '2019-09-09', 1093782976, 54001, 18, 'yuran', 1, '', 'noche', 'realizada', 'realizada', 'realizada', 'No realizada', 'No realizada', 'No realizada'),
(13, '11', '21', '21', 'tecnico', 'activo', '2017-09-09', '3', '2017-09-09', '2018-09-09', '2019-09-09', 1093782976, 54001, 16, '21', 2, '', 'manana', 'realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada'),
(14, 'adsi', '3111', '2323', 'tecnologo', 'activo', '2018-10-10', '3', '2018-10-11', '2018-11-11', '2019-11-11', 1093782976, 54001, 17, '21', 2, '', 'tarde', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada'),
(15, 'adsi', '313', '312', 'tecnico', 'activo', '2017-09-09', '2', '2018-09-09', '2019-09-09', '2020-09-09', 1193469820, 54001, 17, 'yuran', 5, '', 'manana', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada'),
(22, 'adsi', '21', '21', 'tecnico', 'activo', '2018-10-09', '2', '2017-09-09', '2018-09-09', '2019-09-09', 1007029945, 54001, 21, 'yu', 1, '', 'tarde', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada'),
(220, 'addsi', '21', '21', 'tecnico', 'activo', '2019-09-09', '2', '2018-09-09', '2019-09-09', '2020-09-09', 1193469820, 54001, 21, '21', 5, '2', 'manana', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada', 'No realizada'),
(1366055, 'adsi', '12', '12', 'tecnico', 'inactivo', '2018-09-11', '4', '2018-09-11', '2018-09-12', '2020-09-11', 1007029945, 54261, 18, 'yuran', 3, '', 'manana', 'realizada', 'No realizada', 'No realizada', 'realizada', 'No realizada', 'No realizada'),
(1366056, 'analisis y desarrollo de sistemas ', '221', '1.2', 'tecnologo', 'activo', '0000-00-00', '2', '2018-09-26', '2019-06-19', '2020-09-26', 1193469820, 54128, 15, 'yuran nuÃ±ez', 3, '', 'tarde', 'No realizada', 'realizada', 'realizada', 'No realizada', 'No realizada', 'No realizada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_tras`
--

CREATE TABLE `historial_tras` (
  `cod_historial` int(11) NOT NULL,
  `ficha` int(11) NOT NULL,
  `cod_tras` int(11) NOT NULL,
  `horario_tras` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(22) COLLATE utf8_spanish_ci NOT NULL,
  `cod_ambiente` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `fecha_inicio_tras` date NOT NULL,
  `fecha_fin_tras` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `historial_tras`
--

INSERT INTO `historial_tras` (`cod_historial`, `ficha`, `cod_tras`, `horario_tras`, `color`, `cod_ambiente`, `cedula`, `fecha_inicio_tras`, `fecha_fin_tras`) VALUES
(1, 12, 4, 'manana', '#47AB46', 3, 1193469820, '2018-09-10', '2018-09-15'),
(2, 12, 2, 'manana', '#9548AC', 1, 1007029945, '2018-09-18', '2018-09-21'),
(3, 1366056, 4, 'manana', '#47AB46', 4, 1193469820, '2018-09-12', '2018-09-26'),
(4, 1366056, 2, 'noche', '#9548AC', 5, 1193469820, '2018-09-12', '2018-09-26'),
(5, 13, 6, '3', '#FF1419', 1, 1193469820, '2018-10-12', '2018-10-15'),
(6, 13, 6, '3', '#FF1419', 1, 1193469820, '2018-10-12', '2018-10-15'),
(7, 13, 6, '3', '#FF1419', 1, 1193469820, '2018-10-12', '2018-10-15'),
(8, 13, 6, '2', '#FF1419', 5, 1193469820, '2018-10-16', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructor`
--

CREATE TABLE `instructor` (
  `cedula` int(15) NOT NULL,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `apellido` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_municipio` int(15) NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `correo_electronico` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `perfil_profesional` text COLLATE utf8_spanish_ci NOT NULL,
  `cargo` text COLLATE utf8_spanish_ci NOT NULL,
  `tipo_contrato` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio_contrato` date NOT NULL,
  `fecha_fin_contrato` date NOT NULL,
  `estado` text COLLATE utf8_spanish_ci NOT NULL,
  `manana` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tarde` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `noche` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `instructor`
--

INSERT INTO `instructor` (`cedula`, `nombre`, `apellido`, `cod_municipio`, `telefono`, `direccion`, `correo_electronico`, `perfil_profesional`, `cargo`, `tipo_contrato`, `fecha_inicio_contrato`, `fecha_fin_contrato`, `estado`, `manana`, `tarde`, `noche`) VALUES
(1007029945, 'angie', 'carrillo', 54001, '3172865231', 'av 10', 'angie@misna', 'ing sistemas', 'Lider', 'Planta', '2018-09-11', '2019-09-11', 'activo', 'disponible', 'ocupado', 'disponible'),
(1093782976, 'rafael ricardo', 'alvarez', 54344, '3138520124', 'av9', 'ra@m', 'ing sistemas', 'Transversal', 'Planta', '2018-09-13', '2021-09-13', 'activo', 'disponible', 'ocupado', 'disponible'),
(1193469820, 'ana maria', 'vega pacheco', 54001, '3138520124', 'av 9', 'a@misena', 'ing sistemas', 'Lider', 'Planta', '2018-09-12', '2019-09-12', 'activo', 'disponible', 'disponible', 'disponible');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `cod_municipio` int(15) NOT NULL,
  `nombre_municipio` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`cod_municipio`, `nombre_municipio`) VALUES
(54001, 'CUCUTA'),
(54003, 'ABREGO'),
(54051, 'ARBOLEDAS'),
(54099, 'BOCHALEMA'),
(54109, 'BUCARASICA'),
(54125, 'CACOTA'),
(54128, 'CACHIRA'),
(54172, 'CHINACOTA'),
(54174, 'CHITAGA'),
(54206, 'CONVENCION'),
(54223, 'CUCUTILLA'),
(54239, 'DURANIA'),
(54245, 'EL CARMEN'),
(54250, 'EL TARRA'),
(54261, 'EL ZULIA'),
(54313, 'GRAMALOTE'),
(54344, 'HACARI'),
(54347, 'HERRAN'),
(54377, 'LABATECA'),
(54385, 'LA ESPERANZA'),
(54398, 'LA PLAYA'),
(54405, 'LOS PATIOS'),
(54418, 'LOURDES'),
(54480, 'MUTISCUA'),
(54498, 'OCANA'),
(54518, 'PAMPLONA'),
(54520, 'PAMPLONITA'),
(54553, 'PUERTO SANTANDER'),
(54599, 'RAGONVALIA'),
(54660, 'SALAZAR'),
(54670, 'SAN CALIXTO'),
(54673, 'SAN CAYETANO'),
(54680, 'SANTIAGO'),
(54720, 'SARDINATA'),
(54743, 'SILOS'),
(54800, 'TEORAMA'),
(54810, 'TIBU'),
(54820, 'TOLEDO'),
(54871, 'VILLA CARO'),
(54874, 'VILLA DEL ROSARIO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sede`
--

CREATE TABLE `sede` (
  `cod_sede` int(15) NOT NULL,
  `nombre_sede` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `cod_municipio` int(15) NOT NULL,
  `direccion_sede` varchar(69) COLLATE utf8_spanish_ci NOT NULL,
  `estado` text COLLATE utf8_spanish_ci NOT NULL,
  `telefono_sede` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `apoyo_sede` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_sede` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `sede`
--

INSERT INTO `sede` (`cod_sede`, `nombre_sede`, `cod_municipio`, `direccion_sede`, `estado`, `telefono_sede`, `apoyo_sede`, `descripcion_sede`) VALUES
(1, 'a', 54001, 'actualizar', 'inactivo', '3138520124', 'agua', 'ana'),
(2, 'improsistemas', 54001, 'av 9', 'activo', '5712925', '31385201', 'se ubica en la casa'),
(3, 'cedrum', 54250, 'av9', 'activo', '5712925', '3138520124', 'uno dos tres'),
(4, 'improsistemas', 54223, 'av 10', 'inactivo', '571292', '3133', 'ingreso'),
(6, 'hola', 54344, 'khgkjh', 'activo', '', '', ''),
(7, 'duros', 54239, 'fghjk', 'activo', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trasversal`
--

CREATE TABLE `trasversal` (
  `cod_tras` int(11) NOT NULL,
  `nombre_trasversal` text COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(22) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `trasversal`
--

INSERT INTO `trasversal` (`cod_tras`, `nombre_trasversal`, `color`) VALUES
(1, 'ingles', '#CA6466'),
(2, 'etica', '#9548AC'),
(3, 'matematica', '#50ADB5'),
(4, 'comunicacion', '#47AB46'),
(5, 'cultura fisica', '#FF6378'),
(6, 'medio ambiente', '#FF1419');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  ADD PRIMARY KEY (`cod_ambiente`),
  ADD KEY `cod_sede` (`cod_sede`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`login`);

--
-- Indices de la tabla `formacion`
--
ALTER TABLE `formacion`
  ADD PRIMARY KEY (`ficha`),
  ADD KEY `cedula` (`cedula`),
  ADD KEY `cod_municipio` (`cod_municipio`),
  ADD KEY `cod_ambiente` (`cod_ambiente`);

--
-- Indices de la tabla `historial_tras`
--
ALTER TABLE `historial_tras`
  ADD PRIMARY KEY (`cod_historial`),
  ADD KEY `cedula` (`cedula`),
  ADD KEY `cod_ambiente` (`cod_ambiente`),
  ADD KEY `cod_trans` (`cod_tras`);

--
-- Indices de la tabla `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`cedula`),
  ADD KEY `cod_municipio` (`cod_municipio`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`cod_municipio`);

--
-- Indices de la tabla `sede`
--
ALTER TABLE `sede`
  ADD PRIMARY KEY (`cod_sede`),
  ADD KEY `cod_municipio` (`cod_municipio`);

--
-- Indices de la tabla `trasversal`
--
ALTER TABLE `trasversal`
  ADD PRIMARY KEY (`cod_tras`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  MODIFY `cod_ambiente` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `historial_tras`
--
ALTER TABLE `historial_tras`
  MODIFY `cod_historial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `sede`
--
ALTER TABLE `sede`
  MODIFY `cod_sede` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `trasversal`
--
ALTER TABLE `trasversal`
  MODIFY `cod_tras` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ambiente`
--
ALTER TABLE `ambiente`
  ADD CONSTRAINT `ambiente_ibfk_1` FOREIGN KEY (`cod_sede`) REFERENCES `sede` (`cod_sede`);

--
-- Filtros para la tabla `formacion`
--
ALTER TABLE `formacion`
  ADD CONSTRAINT `formacion_ibfk_1` FOREIGN KEY (`cedula`) REFERENCES `instructor` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formacion_ibfk_2` FOREIGN KEY (`cod_municipio`) REFERENCES `municipio` (`cod_municipio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formacion_ibfk_4` FOREIGN KEY (`cod_ambiente`) REFERENCES `ambiente` (`cod_ambiente`);

--
-- Filtros para la tabla `historial_tras`
--
ALTER TABLE `historial_tras`
  ADD CONSTRAINT `historial_tras_ibfk_2` FOREIGN KEY (`cedula`) REFERENCES `instructor` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `instructor`
--
ALTER TABLE `instructor`
  ADD CONSTRAINT `instructor_ibfk_1` FOREIGN KEY (`cod_municipio`) REFERENCES `municipio` (`cod_municipio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sede`
--
ALTER TABLE `sede`
  ADD CONSTRAINT `sede_ibfk_1` FOREIGN KEY (`cod_municipio`) REFERENCES `municipio` (`cod_municipio`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
