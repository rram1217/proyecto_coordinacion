<?php

$id=$_GET['ficha'];

require '../controlador/conexion.php';

	$sql = "SELECT * FROM formacion WHERE ficha=$id	";
	$resultado = $mysqli->query($sql);

?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<title></title>
</head>
<body>
	<head>

	 <meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script>
		$(document).ready(function(){

			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 


				}


			});
		});


	</script>

</head>

<body>
	
	<div class="container">
		<div class="row">

		</div>

		
		<br>
<?php 
	$row = mysqli_fetch_assoc($resultado);

//if($row['ficha'] == $id){
?>
<div class="row table-responsive">
			<table class="verde" width="60%" border="0" cellspacing=20 style="background-color:rgb(255, 255, 255, 0.4);">
	
			<thead>
				<tr>
				<th>FICHA</th>
				<th>FORMACION</th>
				<th>NIVEL</th>
				<th>INSTRUCTOR</th>
				<th>MUNICIPIO</th>
				<th>SEDE</th>
				<th>AMBIENTE</th>
				<th>JORNADA</th>
				<th>MEDIO AMBIENTE</th>
				<th>COMUNICACION</th>
				<th>ETICA</th>
				<th>MATEMATICAS</th>
				<th>CULTURA FISICA</th>
				<th>INGLES</th>
				<th></th>
				</tr>
			</thead>

</body>
</html>

<html lang="es">

		
			<tbody>
				
			<tr>
			<?php 
            if ($row['estado']<>'inactivo'){
				?>
			<td><?php echo $row['ficha']; ?></td>
			<td><?php echo $row['nombre_ficha']; ?></td>
			<td><?php echo $row['nivel']; ?></td>
			<td><?php echo $row['cedula']; ?></td>
			<?php 
				$municipio=$row['cod_municipio'];
				$sql1 = "SELECT * FROM municipio WHERE cod_municipio='$municipio'";
				$resultado1 = $mysqli->query($sql1); 
				$row1 = mysqli_fetch_assoc($resultado1);?>
			<td><?php echo $row1['nombre_municipio']; ?></td>
			<td><?php echo $row['cod_sede']; ?></td>
			<td><?php echo $row['cod_ambiente']; ?></td>
			<td><?php echo $row['jornada']; ?></td>
			<td>
				<?php
				$no="realizada";
				if ($row['medio_ambiente']<>$no)
				{
					?>
				<i class="far fa-calendar-plus"></i>
				<?php }	else {	?><i class="far fa-check-circle"></i>
				<?php }	?>	
			</td>
	
				<td>
			<?php
			$no="realizada";
			if ($row['comunicacion']<>$no)
			{
				?>
				<i class="far fa-calendar-plus"></i>
				<?php }	else {	?><i class="far fa-check-circle"></i>
				<?php }	?>	</td>
						<td>
			<?php
			$no="realizada";
			if ($row['etica']<>$no)
			{
					?>
				<i class="far fa-calendar-plus"></i>
				<?php }	else {	?><i class="far fa-check-circle"></i>
				<?php }	?>	</td>
						<td>
			<?php
			$no="realizada";
			if ($row['matematica']<>$no)
			{
			
					?>
				<i class="far fa-calendar-plus"></i>
				<?php }	else {	?><i class="far fa-check-circle"></i>
				<?php }	?>		
			</td>
						<td>
			<?php
			$no="realizada";
			if ($row['cultura_fisica']<>$no)
			{
					?>
				<i class="far fa-calendar-plus"></i>
				<?php }	else {	?><i class="far fa-check-circle"></i>
				<?php }	?>	
			</td>
			<td>
			<?php
			$no="realizada";
			if ($row['ingles']<>$no)
			{
					?>
				<i class="far fa-calendar-plus"></i>
				<?php }	else {	?><i class="far fa-check-circle"></i>
				<?php }	?>	
			</td>
		<!--				<td>
			<?php
			/*$no="realizada";
			if ($row['otra']<>$no)
			{
				?>
			<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo 'otra'?>&jornada=<?php echo $row['jornada'];?>&op=maxf1" class="glyphicon glyphicon-pencil">asignar transversal</a><?php }	
			  
			else
			{
				echo $row['otra'];	 
				}
	*/		?>	
			</td>-->
	
	</tr>
	<?php
}

?>
</tbody>
</table>
</div>
</div>
</body>
</html>
