<?php 

require '../controlador/conexion.php';

$id=$_GET['id'];
$jornada=$_GET['jornada'];


?>


<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<title></title>
</head>
<body>
	<head>
		<script>
			$(document).ready(function(){
				$('#mitabla').DataTable({
					"language":{
						"lengthMenu": "Mostrar_MENU_Registros por pagina",
						"info": "Mostrando pagina _PAGE_ de _PAGES_",
						"infoEmpty": "No hay registros disponibles",
						"infoFiltered": "(filtrada de _MAX_ registros)",
						"loadingRecords": "Cargando...",
						"Processing": "Procesando...",
						"search": "Buscar:",
						"zeroRecords": "No se encontraron Registros",
						"paginate": {
							"next": "Siguente",
							"previous": "Anterior"
						} 
					}
				});
			});
		</script>
	</head>
<body>
	<div class="container">
		<div class="row">
		</div>
		<br>
<div class="row table-responsive">
			<table class="display" id="mitabla" width="100%" cellspacing=20 style="background-color:rgb(255, 255, 255, 0.4);">
			<thead>
				<tr>
					<th>FICHA</th>
					<th>FORMACION</th>
					<th>NIVEL</th>
					<th>INSTRUCTOR</th>
					<th>MUNICIPIO</th>
					<th>AMBIENTE</th>
					<th>JORNADA</th>
					<th>MEDIO AMBIENTE</th>
					<th>COMUNICACION</th>
					<th>ETICA</th>
					<th>MATEMATICAS</th>
					<th>CULTURA FISICA</th>
					<th>INGLES</th>
			</thead>
</body>
</html>
<?php  

	$sql = "SELECT * FROM formacion WHERE jornada<>'$jornada' and medio_ambiente='No realizada' or  comunicacion='No realizada' or  etica='No realizada' or  matematica='No realizada' or  cultura_fisica='No realizada' or  ingles='No realizada'
	";
	$resultado = $mysqli->query($sql);
	?> 
<html lang="es">		
	<tbody>
		<?php while($row = mysqli_fetch_assoc($resultado)){
        if ($row['estado']<>"inactivo" && $row['jornada'] <> $jornada){
			?>
			<tr>
			<td><?php echo $row['ficha']; ?></td>
			<td><?php echo $row['nombre_ficha']; ?></td>
			<td><?php echo $row['nivel']; ?></td>
			<td><?php echo $row['cedula']; ?></td>
			<td><?php echo $row['cod_municipio']; ?></td>
			<td><?php echo $row['cod_ambiente']; ?></td>
			<td><?php echo $row['jornada']; ?></td>
			<td>
				<?php
				$no="realizada";
				if ($row['medio_ambiente']<>$no)
				{
					?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '6'?>&jornada=<?php echo $row['jornada'];?>&op=asigca"><i class="far fa-calendar-plus"></i></a>
				<?php }	else {	?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '6'?>&op=rea"><i class="far fa-check-circle"></i></a>
				<?php }	?>	
			</td>
			<td>
				<?php
				$no="realizada";
				if ($row['comunicacion']<>$no)
				{
					?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '4'?>&jornada=<?php echo $row['jornada'];?>&op=asigca"><i class="far fa-calendar-plus"></i></a>
				<?php }	else {	?>	
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '4'?>&op=rea"><i class="far fa-check-circle"></i></a>	 
				<?php }	?>	
			</td>
			<td>
				<?php
				$no="realizada";
				if ($row['etica']<>$no)
				{
					?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '2'?>&jornada=<?php echo $row['jornada'];?>&op=asigca"><i class="far fa-calendar-plus"></i></a>
				<?php }	else {	?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '2'?>&op=rea"><i class="far fa-check-circle"></i></a>
				<?php }	?>	
			</td>
			<td>
				<?php
				$no="realizada";
				if ($row['matematica']<>$no)
				{
					?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '3'?>&jornada=<?php echo $row['jornada'];?>&op=asigca"><i class="far fa-calendar-plus"></i></a>
				<?php }	else { ?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '3'?>&op=rea"><i class="far fa-check-circle"></i></a><?php }
				?>	
			</td>
			<td>
				<?php
				$no="realizada";
				if ($row['cultura_fisica']<>$no)
				{
					?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '5'?>&jornada=<?php echo $row['jornada'];?>&op=asigca" ><i class="far fa-calendar-plus"></i></a>
				<?php }	else {	?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '5'?>&op=rea"><i class="far fa-check-circle"></i></a>
				<?php }	?>	
			</td>
			<td>
				<?php
				$no="realizada";
				if ($row['ingles']<>$no)
				{
					?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '1'?>&jornada=<?php echo $row['jornada'];?>&op=asigca"><i class="far fa-calendar-plus"></i></a>
				<?php }	else {	?>
					<a href="index.php?id=<?php echo $row['ficha'];?>&trans=<?php echo '1'?>&op=rea"><i class="far fa-check-circle"></i></a><?php }	
				?>	
			</td>
			
	</tr>
	<?php
}	
}
?>
</tbody>
</table>
</div>
</div>
</body>
</html>
