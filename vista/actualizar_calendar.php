<?php
require '../controlador/conexion.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Proyecto Coordinacion</title>
	    <link rel="icon" href="../img/logoSena.ico">
	<link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<script src="js/jquery.min.js"></script>
	<script src="js/moment.min.js"></script>
	<link rel="stylesheet"  href="css/fullcalendar.min.css">
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/es.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" ></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" ></script>

</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col"></div>
			<div class="col-8">	<div id="calendarioweb"></div></div>
			<div class="col"></div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			$('#calendarioweb').fullCalendar({
				header:{
						  left:   'today prev,next',
						  center: 'title',
						  right:  'month'
						},
				customButtons:{
					miboton:{
							text:"Boton 1",
							click:function(){
							alert("accion del boton");
						}
					}
				},
				dayClick:function(date,jsEvent,view){
					//alert("valor selecionado: "+date.format());
					//alert("vista: "+view.name);
					//$(this).css('background-color','gray');
					$("#start").val(date.format());
					$("#actualizar").modal();

				},
				<?php $sql = "SELECT * FROM historial_tras ";
                    $resultado = $mysqli->query($sql);
                      while ($row = mysqli_fetch_array($resultado))
                        {
                        	$codigo=$row['cod_historial'];
                        	$ficha=$row['ficha'];
                        	$codigo_tra=$row['cod_tras'];
                        	$horario_tras=$row['horario_tras'];
                        	$ambiente=$row['cod_ambiente'];
                        	$cedula=$row['cedula']; 
                        	$color=$row['color'];
                        	$inicio=$row['fecha_inicio_tras'];
                        	$fin=$row['fecha_fin_tras']; 

                        ?>
//------------------------------------------------------------------------------------------------------

				<?php $sql = "SELECT * FROM trasversal where cod_tras='$codigo_tra' ";
                    $resultado = $mysqli->query($sql);
                      while ($row = mysqli_fetch_array($resultado))
                        {
                        	$nombre_tras=$row['nombre_trasversal'];
                       	}
                        ?>
//------------------------------------------------------------------------------------------------------

				<?php $sql = "SELECT * FROM ambiente where cod_ambiente='$ambiente' ";
                    $resultado = $mysqli->query($sql);
                      while ($row = mysqli_fetch_array($resultado))
                        {
                        	$nombre_amb=$row['ambiente'];
                       	}
                        ?>
//------------------------------------------------------------------------------------------------------
                        <?php $sql = "SELECT * FROM formacion WHERE ficha='$ficha' ";
                    $resultado = $mysqli->query($sql);
                      while ($row = mysqli_fetch_assoc($resultado))
                        {
                        	$nombre_forma=$row['nombre_ficha'];
                        }
                        ?>
//------------------------------------------------------------------------------------------------------

                        <?php $sql = "SELECT * FROM instructor WHERE cedula='$cedula' ";
                    $resultado = $mysqli->query($sql);
                      while ($row = mysqli_fetch_assoc($resultado))
                        {
                        	$nombre_=$row['nombre'];
                        }}
                        ?>
//------------------------------------------------------------------------------------------------------

					events: [
					    <?php $sql = "SELECT * FROM historial_tras ";
                    $resultado = $mysqli->query($sql);
                      while ($event = mysqli_fetch_array($resultado))
                        { 
			
				?>
				{
					id: '<?php echo $event['cod_historial']; ?>',
					title: '<?php echo $event['ficha']; ?>'+" - "+'<?php echo $nombre_forma; ?>'+" - "+'<?php echo $nombre_tras; ?>'+" - "+'<?php echo $nombre_amb; ?>'+" - "+'<?php echo $nombre_; ?>'+" - "+'<?php echo $event['horario_tras']; ?>',
					ficha:'<?php echo $event['ficha']; ?>',
					trasversal:'<?php echo $event['cod_tras']; ?>',
					horario:'<?php echo $event['horario_tras']; ?>',
					ambiente:'<?php echo $event['cod_ambiente']; ?>',
					instructor:'<?php echo $event['cedula']; ?>',
					color: '<?php echo $event['color']; ?>',
					start: '<?php echo $event['fecha_inicio_tras']; ?>',
					end: '<?php echo $event['fecha_fin_tras']; ?>',
				},

			<?php } ?>
					  ],

					 eventRender: function(calEvent, element) {
				element.bind('dblclick', function() {
					$('#actualizar #ficha').val(calEvent.ficha);
					$('#actualizar #tras').val(calEvent.trasversal);
					$('#actualizar #id').val(calEvent.id);
					$('#actualizar #hora').val(calEvent.horario);
					$('#actualizar #amb').val(calEvent.ambiente);
					$('#actualizar #ins').val(calEvent.instructor);
					$('#actualizar #color').val(calEvent.color);
					$('#actualizar #start').val(calEvent.start);
					$('#actualizar #end').val(calEvent.end);

					$('#actualizar').modal('show');
				});
			}
					  

				/*eventClick:function(calEvent,jsEvent,view)
				{
					$('#tras').html(calEvent.trasversal);
					$("#actualizar").modal();
				}*/
						
			});

			});
	</script>
	<?php 

	?>
<div class="modal fade" id="actualizar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="../controlador/editEventTitle.php">
			
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Actualizar Transversal</h4>
			  </div>
			  <div class="modal-body">

			  <input type="hidden" name="id" class="form-control" id="id">

				
				<div class="form-group">
					<label for="ficha" class="col-sm-2 control-label">Numero Ficha</label>
					<div  class="col-sm-10">
					  <input type="number" name="ficha" class="form-control" id="ficha" readonly >
					</div>
				  </div>


				  <div class="form-group">
					<label for="tras"  class="col-sm-2 control-label">Transaversal</label>
					<div class="col-sm-10">
					<select name="tras" class="form-control" id="tras">
                        <?php
                        $sql = "SELECT * FROM trasversal ";
                    $resultado = $mysqli->query($sql);
                      while ($row = mysqli_fetch_array($resultado))
                        {
                            echo "<option value='".$row['cod_tras']."' >".$row['nombre_trasversal']."</option>";
                    	}
                        ?>
                    </select>					</div>
				  </div>

				  <div class="form-group">
					<label for="hora" class="col-sm-2 control-label">Horario</label>
					<div class="col-sm-10">
					<select name="hora" class="form-control" id="hora">
									  <option value="">Seleccionar</option>
						  <option  value="manana">Mañana</option>
						  <option  value="tarde">Tarde</option>
						  <option  value="noche">Noche</option>				  
						  						  
						</select>
                    	</div>
				  </div>
				  <div class="form-group">
					<label for="ambiente" class="col-sm-2 control-label">Ambiente</label>
					<div class="col-sm-10">
					<select name="ambiente" class="form-control" id="amb">
                        <?php
                        $sql = "SELECT * FROM ambiente ";
                    $resultado = $mysqli->query($sql);
                      while ($row = mysqli_fetch_array($resultado))
                        {
                            echo "<option value='".$row['cod_ambiente']."' >".$row['ambiente']."</option>";
                    	}
                        ?>
                    </select>					</div>
				  </div>
				  <div class="form-group">
					<label for="doc_in" class="col-sm-2 control-label">Intructor</label>
					<div class="col-sm-10">
					<select name="doc_in" class="form-control" id="ins">
                        <?php
                        $sql = "SELECT * FROM instructor ";
                    $resultado = $mysqli->query($sql);
                      while ($row = mysqli_fetch_array($resultado))
                        {
                            echo "<option value='".$row['cedula']."' >".$row['nombre']."</option>";
                    	}
                        ?>
                    </select>					</div>
				  </div>
				  <div class="form-group">
					<label for="color" class="col-sm-2 control-label">Color</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
									  <option value="">Seleccionar</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
						  <option style="color:#008000;" value="#008000">&#9724; Verde</option>				  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
						  <option style="color:#000;" value="#000">&#9724; Negro</option>
						  
						</select>
					</div>
				  </div>
				  <div class="form-group">
					<label for="start" class="col-sm-2 control-label">Fecha Inicial</label>
					<div class="col-sm-10">
					  <input type="date" name="start" class="form-control" id="start" >
					</div>
				  </div>
				  <div class="form-group">
					<label for="end" class="col-sm-2 control-label">Fecha Final</label>
					<div class="col-sm-10">
					  <input type="date" name="end" class="form-control" id="end" >
					</div>
				  </div>
				
			  </div>
			  <div class="modal-footer">
			  	<button type="submit"  class="btn btn-primary">Actualizar</button>
				<button type="button"  class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>


		

</body>
</html>