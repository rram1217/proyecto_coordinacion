<?php
	require '../controlador/conexion.php';
	
	$id = $_GET['id'];
	
	$sql = "SELECT * FROM historialtras WHERE cod_historial = '$id'";
	$resultado = $mysqli->query($sql);
	$row = mysqli_fetch_assoc($resultado);
	
?>

<html lang="es">
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../modelo/css/bootstrap.min.css" rel="stylesheet">
		<link href="../modelo/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../modelo/js/jquery-3.1.1.min.js"></script>
		<script src="../modelo/js/bootstrap.min.js"></script>	

		<script type="text/javascript">
			$(document).ready(function(){
				$('.delete').click(function(){
					var parent = $ (this).parent().attr('id');
					var service = $ (this).parent().attr('data');
					var dataString = 'id='+service;

					$.ajax({
						type: "POST",
						url: "del_file.php",
						data: dataString,
						success: function(){
							location.reload();
						}
					});
				});
			});
		</script>
	</head>
	
	<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">MODIFICAR REGISTRO</h3>
			</div>
			
			<form class="form-horizontal" method="POST" action="../controlador/b.php" enctype="multipart/form-data" autocomplete="off">
				<div class="form-group">
					<label class="col-sm-2 control-label">Codigo de Transversal</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="codigo" name="codigo" placeholder="Codigo" value="<?php echo $row['cod_tras']; ?>" required>
					</div>
				</div>
				<input type="hidden" id="fi" name="fi" value="<?php echo $row['ficha']; ?>" />

				<input type="hidden" id="id" name="id" value="<?php echo $row['cod_historial']; ?>" />

				<div class="form-group">
					<label  class="col-sm-2 control-label">Horario de Transversal</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hora" name="hora" placeholder="Horario" value="<?php echo $row['horario_tras']; ?>" required>
					</div>
				</div>
				
				<div class="form-group">
					<label  class="col-sm-2 control-label">Codigo de Ambiente</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="cod_amb" name="cod_amb" placeholder="Codigo" value="<?php echo $row['cod_ambiente']; ?>"  required>
					</div>
				</div>
				
				<div class="form-group">
					<label  class="col-sm-2 control-label">Codigo de Instructor</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="cod_ins" name="cod_ins" placeholder="Codigo" value="<?php echo $row['cod_ins']; ?>" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Fecha Inicial</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="ini" name="ini" placeholder="Fecha Inicial" value="<?php echo $row['fecha_i_tras']; ?>" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Fecha Final</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="fin" name="fin" placeholder="Fecha Final " value="<?php echo $row['fecha_fin_tras']; ?>" >
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="actu_trans.php?url=123" class="btn btn-default">Regresar</a>
						<button type="submit" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			