<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body>
	<head>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
		</div>
		<div class="row table-responsive">
			<table class="verde" id="mitabla" style="background-color:rgb(255, 255, 255, 0.4);">
			<thead>
				<tr>
				<th>FICHA</th>
				<th>FORMACION</th>
				<th>NIVEL</th>
				<th>FECHA DE INICIO</th>
				<th>FECHA FIN</th>
				<th>FECHA FIN LECTIVA</th>
				<th>INSTRUCTOR</th>
				<th>MUNICIPIO</th>
				<th>N° DE APRENDICES</th>
				<th>LIDER</th>
				<th>AMBIENTE</th>
				<th>JORNADA</th>
				<th>MEDIO AMBIENTE</th>
				<th>COMUNICACION</th>
				<th>ETICA</th>
				<th>MATEMATICAS</th>
				<th>CULTURA FISICA</th>
				<th>INGLES</th>
				<th>EDITAR</th>
				</tr>
			</thead>
</body>
</html>
<?php  
require '../controlador/conexion.php';
	$sql = "SELECT * FROM formacion ";
	$resultado = $mysqli->query($sql);				
	?> 
<html lang="es">
			<tbody>
			<tr>
			<?php while($row = mysqli_fetch_assoc($resultado)){?>
			<td><?php echo $row['ficha']; ?></td>
			<td><?php echo $row['nombre_ficha']; ?></td>
			<td><?php echo $row['nivel']; ?></td>
			<td><?php echo $row['fecha_inicio']; ?></td>
			<td><?php echo $row['fecha_fin_formacion']; ?></td>
			<td><?php echo $row['fecha_fin_lectiva']; ?></td>
			<td><?php 
				$r=$row['cedula'];
			$result2=mysqli_query($mysqli, "SELECT * FROM instructor WHERE cedula='$r'");
			mysqli_data_seek ($result2, 0);
			$extraido = mysqli_fetch_array($result2);
			$nombre=$extraido['nombre'];
			$apellido=$extraido['apellido'];
			echo $nombre; echo " "; echo $apellido; ?>
			</td>
			<td><?php 
			$r=$row['cod_municipio'];
			$result2=mysqli_query($mysqli, "SELECT * FROM municipio WHERE cod_municipio='$r'");
			mysqli_data_seek ($result2, 0);
			$extraido = mysqli_fetch_array($result2);
			$nombre=$extraido['nombre_municipio'];
			echo $nombre; ?>
			</td>


			<td><?php echo $row['n_aprendices']; ?></td>
			<td><?php echo $row['aprendiz_lider']; ?></td>
			<td><?php 
			$r=$row['cod_ambiente'];
			$result2=mysqli_query($mysqli, "SELECT * FROM ambiente WHERE cod_ambiente='$r'");
			mysqli_data_seek ($result2, 0);
			$extraido = mysqli_fetch_array($result2);
			$nombre=$extraido['ambiente'];
			echo $nombre; ?>
			</td>
			<td><?php echo $row['jornada']; ?></td>
			<td><?php echo $row['medio_ambiente']; ?></td>
			<td><?php echo $row['comunicacion']; ?></td>
			<td><?php echo $row['etica']; ?></td>
			<td><?php echo $row['matematica']; ?></td>
			<td><?php echo $row['cultura_fisica']; ?></td>
			<td><?php echo $row['ingles']; ?></td>
			<td><a href="index.php?id=<?php echo $row['ficha'];?>&op=act_forma"><i class="fas fa-pencil-alt"></i></a></td>
	</tr>
	<?php
}	
?>
</tbody>
</table>
</div>
</div>
</body>
</html>
