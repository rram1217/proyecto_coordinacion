<?php 
session_start(); 
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) { 
/*<?php 
$url = isset($_POST['url']);
$url = isset($_GET['url']) ? $_GET['url'] : 0;
if ($url == 123)
{*/
	?>
	<!doctype html>
	<html lang="es">
	  <head>
	  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	    <!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta http-equiv="refresh" content="900;URL=login.php">
	    <!-- Bootstrap CSS -->
	    <link rel="stylesheet" type="text/css" href="../modelo/css/interactiva.css">
	    <link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
		<link rel="stylesheet" type="" href="../modelo/css/bootstrap.css">
		<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
		<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
		<script src="../modelo/js/interactiva.js"></script>
		<script src="../modelo/js/jQuery-3.3.1.js"></script>
		<script src="../modelo/js/jquery-3.1.1.min.js"></script>
		<script src="../modelo/js/bootstrap.min.js"></script>
		<script src="../modelo/js/jquery.dataTables.min.js"></script>
<!--
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
	    
	    <title>COORDINACION ACADEMICA</title>
	    <link rel="icon" href="../img/logoSena.ico">
	    <!--<audio autoplay src=""></audio>-->
	  </head>
	  <!--<FRAMESET rows="100%" BORDER=0 FRAMEBORDER=0 FRAMESPACING=0> 
	<FRAME NAME="top" SRC="index.php" NORESIZE></FRAMESET>-->
	  <body>
		<div class="container-fluid">
			<center><img src="../img/banner1.png" class="img-fluid" alt="Responsive image" width= "100%" height="auto"></center>
		</div>
	    <div class="container-fluid">
	          <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	              <a class="navbar-brand" href="index.php?url=123"> <font style="vertical-align: inherit;"><img id ="logo" src="../img/logo.png"> Coordinacion </font></a>
	              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label ="Toggle navigation">
	              <span class="navbar-toggler-icon"></span>
	              </button>
	                <div class="collapse navbar-collapse" id="navbarColor01">
	                    <ul class="navbar-nav mr-auto">
							<li><div class="btn-group" role="group" aria-label="Button group with nested dropdown">
								  <!--<button type="button" class="btn btn-primary">Inicio</button>-->
								  <div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Instructor</button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
									  <a class="dropdown-item" href="index.php?op=in1&url=123">Registrar</a>
									  <a class="dropdown-item" href="index.php?op=in2&url=123">Consulta Basica</a>
									  <a class="dropdown-item" href="index.php?op=in3&url=123">Consulta Maxima</a>
									</div>
								  </div>
								  <div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Formacion</button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
									  <a class="dropdown-item" href="index.php?op=fo1&url=123">Registrar</a>
									  <a class="dropdown-item" href="index.php?op=fo2&url=123">actualizar</a>
									  <a class="dropdown-item" href="index.php?op=fo3&url=123">Consultar</a>
									  <a class="dropdown-item" href="index.php?op=fo4&url=123">Consultar inactivos</a>
									</div>
								  </div>
								  <div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Transversal</button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
									  <a class="dropdown-item" href="index.php?op=tr1&url=123">Registrar</a>
									  <a class="dropdown-item" href="index.php?op=tr4&url=123">Consultar</a>
									</div>
								  </div>
								  <div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ambiente</button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
									  <a class="dropdown-item" href="index.php?op=am1&url=123">Registrar</a>
									  <a class="dropdown-item" href="index.php?op=am3&url=123">Consultar</a>
									</div>
								  </div>
								  <div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sede</button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
									  <a class="dropdown-item" href="index.php?op=se1&url=123">Registrar</a>
									  <a class="dropdown-item" href="index.php?op=se3&url=123">Consultar</a>
										
									</div>

								  </div>
								
								</div>

							</li>

	                    </ul>
	                    <a href="../manual/index.htm"  target="_blank" class="btn-primary"><i class="far fa-question-circle"></i></a>
	                </div>
	          </nav>
	    </div><br><br>
	    <div class="container">
			<center>
			<?php
			if(empty($_GET['op'])==true){
					?><table>
					<tr><td height="400"></td></tr>
					</table>
					<?php
			} 
			if(empty($_GET['op'])==false && $_GET['op'] == 'in1'){
					include('regis_inst.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'in2'){
					include('actu_ins.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'in3'){
					include('consul_ins.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'fo1'){
					include('regis_forma.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'fo2'){
					include('actu_forma_1.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'fo3'){
					include('consul_forma.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'tr1'){
					include('regis_trans.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'tr1_1'){
					include('men_trans.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'tr3'){
					include('actu_trans.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'tr4'){
					include('consul_trans.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'am1'){
					include('regis_ambiente.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'am2'){
					include('actu_ambiente.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'am4'){
					include('asignar_ambiente.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'am3'){
					include('consul_ambiente.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'se1'){
					include('regis_sede.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'se2'){
					include('actu_sede.html');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'se3'){
					include('consul_sede.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'ins6'){
					include('../controlador/consultar_instructor.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'actuins1'){
					include('actu_ins1.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'a1'){
					include('a.php');
			}
			 if(empty($_GET['op'])==false && $_GET['op'] == 'conform'){
					include('../controlador/formacion2.php');
			}
			 if(empty($_GET['op'])==false && $_GET['op'] == 'contran1'){
					include('../controlador/consul_trans1.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'conamb1'){
					include('../controlador/ambiente2.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'consed1'){
					include('../controlador/sede2.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'actuam1'){
					include('../controlador/actu_ambiente2.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'actuse2'){
					include('actu_sede_2.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'act_forma'){
					include('actu_forma2.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'maxf1'){
					include('max_formacion1.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'rea'){
					include('realizada.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'con_inst'){
			include('../controlador/con_form.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'maxf2'){
			include('max_formacion3.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'asigit'){
			include('asignar_ins_trans.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'asigca'){
			include('asignar_calendar.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'act_trans'){
			include('actu_trans.php');
			}
			if(empty($_GET['op'])==false && $_GET['op'] == 'fo4'){
			include('consul_forma_i.html');
			}
			?>
			<table>
				<tr><td height="250"></td></tr>
			</table>
	        </center>
	    </div>
	    <div class="container-fluid">
	    	<nav class="navbar navbar-expand-lg navbar-dark bg-primary"></nav>
			<center><b>Aplicacion creada por el Grupo de ADSI 1366055-A  - Derechos Reservados 2018	</b></center>
			
		</div>
	    <!-- Optional JavaScript -->
	    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	    
	  </body>
	</html>
	<?php
	} else { 
   	echo "<script>alert('Su Sesion a terminado');</script>";
	echo "<script> location.href='login.php'</script>"; 
exit; 
} 
$now = time(); 
if($now > $_SESSION['expire']) { 
session_destroy(); 
echo "Su sesion a terminado, 
<a href='login.php'>Necesita Hacer Login</a>"; 
exit;

}							
?>